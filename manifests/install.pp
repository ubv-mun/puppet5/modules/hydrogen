# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include hydrogen::install
class hydrogen::install {
  package { $hydrogen::package_name:
    ensure => $hydrogen::package_ensure,
  }
}
